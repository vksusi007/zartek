import 'dart:convert';

import 'package:http/http.dart' as http;

class ApiService
{
  var _baseUrl = "https://www.mocky.io/v2/5dfccffc310000efc8d2c1ad";

  getHomeFeed() async
  {
    try{
      var response =  await http.get(Uri.parse(_baseUrl));
      if(response.statusCode == 200)
        {
          var decodeResponse = jsonDecode(response.body);
          return decodeResponse;
        }

      else{
        return response.toString();
      }
    }
    catch(err)
    {
      print(err);
    }
  }
}