import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zartek/models/restaurent_model.dart';

class FeedImage extends StatefulWidget
{
  CategoryDishes? categoryDishes;
  FeedImage({this.categoryDishes}) : super();
  @override
  FeedImageState createState()=> FeedImageState();
}

class FeedImageState extends State<FeedImage>
{
  @override
  Widget build(BuildContext context)
  {
   return Container(
     child: CachedNetworkImage(
       imageUrl: "http://via.placeholder.com/200x150",
       imageBuilder: (context, imageProvider) => Container(
         decoration: BoxDecoration(
           image: DecorationImage(
               image: imageProvider,
               fit: BoxFit.cover,
               ),
         ),
       ),
       placeholder: (context, url) => CircularProgressIndicator(),
       errorWidget: (context, url, error) => Icon(Icons.error, color: Colors.black,),
     ),
     // child: Image.network(widget.categoryDishes!.dishImage.toString(),
     //  width: 50,
     //  height: 50,
     // fit: BoxFit.cover,),
   );
  }

}