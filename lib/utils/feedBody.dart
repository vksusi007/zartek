import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zartek/models/restaurent_model.dart';

class FeedBody extends StatefulWidget
{
  CategoryDishes? categoryDishes;
  FeedBody({this.categoryDishes}) : super();
  @override
  FeedBodyState createState() => FeedBodyState();
}
class FeedBodyState extends State<FeedBody>
{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.categoryDishes!.dishDescription.toString(),
          style: TextStyle(
            color: Colors.grey
          ),),
          SizedBox(height: 10.0,),
          Container(
            width: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: Colors.green
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(onPressed: ()
                {

                }, icon: Icon(Icons.add, color: Colors.white,)),
                Text("1", style: TextStyle(color: Colors.white),),
                IconButton(onPressed: ()
                {

                },
                    icon: Icon(Icons.remove, color: Colors.white,))
              ],
            ),
          ),
          SizedBox(height: 10.0,),
          widget.categoryDishes!.addonCat!.isNotEmpty?
              Text("Customization Available",
               style: TextStyle(color: Colors.red),):Container(),
        ],
      ),
    );
  }

}