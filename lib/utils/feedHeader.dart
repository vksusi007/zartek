import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zartek/models/restaurent_model.dart';

class FeedHeader extends StatefulWidget
{
  TableMenuList? tableMenuList;
  CategoryDishes? categoryDishes;
  FeedHeader({this.tableMenuList, this.categoryDishes}): super();
  @override
  FeedHeaderState createState()=> FeedHeaderState();
}
class FeedHeaderState extends State<FeedHeader>
{
  @override
  Widget build(BuildContext context)
  {
    return Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0),
     child: Column(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: [
         Text(widget.categoryDishes!.dishName.toString(),
         style: TextStyle(
           fontSize: 16.0,
           fontWeight: FontWeight.bold
         ),),
         SizedBox(height: 5.0,),
         Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: [
             Text("${widget.categoryDishes!.dishCurrency.toString()} ${widget.categoryDishes!.dishPrice.toString()}",
             style: TextStyle(color: Colors.black, fontSize: 12.0,
                 fontWeight: FontWeight.bold),),
             SizedBox(width: 20.0,),
             Text("${widget.categoryDishes!.dishCalories.toString()}  Calories",
               style: TextStyle(color: Colors.black,fontSize: 12.0,
                   fontWeight: FontWeight.bold),),
           ],
         )
       ],
     ),
    );
  }

}