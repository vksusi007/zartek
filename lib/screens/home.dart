import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zartek/models/restaurent_model.dart';
import 'package:zartek/services/authentication.dart';
import 'package:zartek/services/repository.dart';
import 'package:zartek/utils/feedBody.dart';
import 'package:zartek/utils/feedHeader.dart';
import 'package:zartek/utils/feed_image.dart';

class Home extends StatefulWidget
{
  User? user;
  Home({this.user}):super();
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home>
{
  List<TableMenuList>? tableList;
  int tabIndex = 0;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    getFeeds();
  }
  @override
  Widget build(BuildContext context)
  {
    return tableList != null?DefaultTabController(
      length: tableList!.length,
      child: Scaffold (
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 3.0,
          leading: IconButton(
            icon: Icon(Icons.menu,
             color: Colors.black,),
            onPressed: (){
               _scaffoldKey.currentState!.openDrawer();
            },
          ),
          actions: [
            IconButton(
                onPressed: (){

                  },
                icon: Icon(Icons.shopping_cart_outlined,
                color: Colors.grey,))
          ],
          bottom:
          TabBar(
            isScrollable: true,
            indicatorColor: Colors.red,
            labelColor:  Colors.red,
            unselectedLabelColor:Colors.black,
            onTap: (int){
              setState(() {
                tabIndex = int;
              });
            },
            tabs: tableList!.asMap().map((index, eachTab) =>
              MapEntry(index,
             Padding(
               padding: const EdgeInsets.only(bottom: 10.0),
               child: Text(eachTab.menuCategory.toString(),
                ),
             ))).values.toList()
          ),
        ),
        body: DefaultTabController(
          length: tableList!.length,
          child: ListView.builder(
              itemCount: tableList?[tabIndex].categoryDishes!.length,
              itemBuilder: (context, index)
              {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.0,),
                    Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FeedHeader(
                            tableMenuList: tableList?[tabIndex],
                            categoryDishes:
                            tableList?[tabIndex].categoryDishes?[index]
                          ),
                          FeedImage(categoryDishes: tableList?[tabIndex].categoryDishes?[index],)
                        ],
                      ),
                    ),
                    FeedBody(categoryDishes: tableList?[tabIndex].categoryDishes?[index],),
                    Divider(color: Colors.black54,)
                  ],
                );
              }),
        ),
        drawer: drawerWidget(),
      ),
    ): Container();
  }

  void getFeeds() async
  {
    var result = await Repository().fetchHomeFeed();
    if(result is RestaurentModel)
      {
        setState(() {
          tableList = result.tableMenuList;
        });
      }
  }

   drawerWidget()
  {
    return Drawer(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: 200,
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15.0),
                 bottomRight: Radius.circular(15.0)),
              ),
              child: widget.user!=null?
                 Center(
                   child: Column(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                       CircleAvatar(
                           backgroundImage: NetworkImage(widget.user!.photoURL.toString()),
                           radius: 30,),
                       SizedBox(height: 10.0,),
                       Text(widget.user!.displayName.toString(),
                         style: TextStyle(color: Colors.white),),

                     ],
                   ),
                 ):Container()
            ),
            SizedBox(height: 10.0,),
            InkWell(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Row(
                  children: [
                    Icon(Icons.logout, color: Colors.grey,),
                     SizedBox(width: 20,),
                     Text("Log Out", style: TextStyle(color: Colors.grey, fontSize: 16.0,
                     fontWeight: FontWeight.bold),)
                  ],
                ),
              ),
              onTap: () async{
                await Authentication.signOut(context: context);
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      ),
    );
  }

}