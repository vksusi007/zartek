import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zartek/screens/home.dart';
import 'package:zartek/services/authentication.dart';

class LoginPage extends StatefulWidget
{
  @override
  LoginPageState createState()=> LoginPageState();
}

class LoginPageState extends State<LoginPage>
{
  @override
  void initState() {
    super.initState();
    initFirebase();
  }
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Image.asset("images/zartek-fire.png",
                   width: 200,
                   height: 200,
                   fit: BoxFit.cover),
            ),
            SizedBox(height: 100,),
            TextButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage("images/zartek-fire.png"),
                      backgroundColor: Colors.blue,
                      radius: 15.0,
                    ),
                    Spacer(),
                    Text("Google",
                        style: TextStyle(color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0)),
                    Spacer(),
                  ],
                ),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                    padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            side: BorderSide(color: Colors.blue)
                        )
                    )
                ),
                onPressed: () async {
                  User? user =
                      await Authentication.signInWithGoogle(context: context);
                  if(user != null)
                    {
                      Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>
                          Home(user:  user,)));
                    } else{
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Google Sign in Failed")));
                  }
                }
            ),
            SizedBox(height: 20,),
            TextButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage("images/zartek-fire.png"),
                      backgroundColor: Colors.green,
                      radius: 15.0,
                    ),
                    Spacer(),
                    Text("Phone",
                        style: TextStyle(color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0)),
                    Spacer(),
                  ],
                ),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                    padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            side: BorderSide(color: Colors.green)
                        )
                    )
                ),
                onPressed: () {
                  Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>
                   Home()));
                }
            ),
          ],
        ),
      ),
    );
  }

  void initFirebase() async
  {
    FirebaseApp firebaseApp = await Firebase.initializeApp();
  }

}